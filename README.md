<div align="center">
    <br />
    <a href="https://www.civitasconnect.digital/produktkatalog/urban-data-platform/">
      <img width="350px" src="https://www.civitasconnect.digital/wp-content/uploads/2024/03/CIVITASCORE_1-zeilig-mit-Icon-oben.png" alt="Docusaurus">
    </a>
    <br />
    <br />
</div>


[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)


This repository contains issue templates for creating different types of issues for [CIVITAS/CORE](https://gitlab.com/civitas-connect/civitas.core/civitas-core). The current state of development can be followed on our [issue board](https://gitlab.com/groups/civitas-connect/civitas-core/-/boards).

## How to use
When you want to create an issue in one of the repositories, select a template from the list and fill it out as described.


## How to Contribute
You found a bug, want to report an issue or add code? [-> Contribution Guide](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/CONTRIBUTION-GUIDE.md)

## Contributors
[-> List of Contributors](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/CONTRIBUTORS.md)

## Reporting Security Issues
Please do not open issues or pull requests - this makes the problem immediately visible to everyone, including malicious actors. Security issues in this open source project can be safely reported via [core@civitasconnect.digital](mailto:core@civitasconnect.digital).

## License
This work is licensed under [EU PL 1.2](LICENSE) by Civitas Connect e. V., Hafenweg 7, 48155 Münster, Germany, and [other authors](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/AUTHORS-ATTRIBUTION.md). 

This project doesn't require a CLA (Contributor License Agreement). The copyright belongs to all the individual contributors.

Please see [here](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/AUTHORS-ATTRIBUTION.md) for previous works this product is based on.
