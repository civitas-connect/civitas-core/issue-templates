## User stories
1. As a \<usergroup> I want to \<user goal>, in order to \<purpose>.
2. ...

## Description of needs and purpose


## Acceptance criteria

_**Tip**: Write a **checkboxed** list of sentences in the present tense describing the desired state of the system._

**GOOD Example (clear how to verify, it reports design and interaction decisions
agreed):**

* [ ] The page shows a button only to logged in users
* [ ] The button has the style XXX and label "Logout"
* [ ] When pressed, opens a confirmation modal with "OK" and "Cancel" buttons
* [ ] ...

**BAD Example (cannot be activated during verification, too arbitrary):**

* [ ] Logout button
* [ ] Modal for confirmation

/label ~type::feature_request
/assign @cr0ssing